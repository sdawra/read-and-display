//Header Files
#include<iostream>
#include<unistd.h>
#include<stdio.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<linux/i2c.h>
#include<linux/i2c-dev.h>

#define Buffer_size 19

int bcdtodec(char b) {
return (b/16)*10 + (b%16); //BCD to decimal conversion 
}

using namespace std;

class Connection{    //making a class

private:
int file;

public:
int Test();
int Sensor();
int Reset();
int Display();
int Write();
int Alarm();
int SQWave();

protected:
char writeBuffer[8];
};

int Connection::Test(){                               
cout<<"Starting the DS3231 test application\n"<<endl;
if((file=open("/dev/i2c-1", O_RDWR)) < 0){
perror("failed to open the bus\n");
return 1;
}
}

int Connection::Sensor(){
if(ioctl(file, I2C_SLAVE, 0x68) < 0){        //input output control on 0x68
perror("Failed to connect to the sensor\n");
return 1;
}
}

int Connection::Reset(){
char writeBuffer[1] = {0x00};              //writing onto 0x00
if(write(file, writeBuffer, 1)!=1){
perror("Failed to reset the read address\n");
return 1;
}
}
   
int Connection::Display(){   
char buf[Buffer_size];
if(read(file, buf, Buffer_size)!=Buffer_size){   //reading the file
perror("Failed to read in the buffer\n");
return 1;
}
cout<<"The RTC time is\n"<<bcdtodec(buf[2])<<":"<<bcdtodec(buf[1])<<":"<<bcdtodec(buf[0])<<endl; //displaying the time at output
cout<<"The RTC date is\n"<<bcdtodec(buf[4])<<"-"<<bcdtodec(buf[5])<<"-"<<bcdtodec(buf[6])<<endl; //displaying the date at output
return 0;
}

// writing onto buffers 0x00 - 0x06 to write date and time
int Connection::Write(){
writeBuffer[1] = {0x00}; //address location 0x00
writeBuffer[1] = 0x00;  //seconds
writeBuffer[2] = 0x50;  //minutes
writeBuffer[3] = 0x18;  //hours
writeBuffer[4] = 0x02;  //day of week
writeBuffer[5] = 0x18;  //date
writeBuffer[6] = 0x03;  //month
writeBuffer[7] = 0x19;  //year

if(write(file, writeBuffer, 1)!=1){
perror("Failed to write in the buffer\n");
return 1;
}
cout<<"The RTC time is\n"<<bcdtodec(writeBuffer[3])<<":"<<bcdtodec(writeBuffer[2])<<":"<<bcdtodec(writeBuffer[1])<<endl;
cout<<"The RTC date is\n"<<bcdtodec(writeBuffer[4])<<" "<<bcdtodec(writeBuffer[5])<<"-"<<bcdtodec(writeBuffer[6])<<"-"<<bcdtodec(writeBuffer[7])<<endl;
write(file,writeBuffer,8);
read(file,writeBuffer,8);
return 0;
}

//Writing the values in the 56 byte of NVRAM starting from 0x08
int Connection::Alarm(){
writeBuffer[1] = {0x08}; //address location to access NVRAM
writeBuffer[1] = 0x00;   //seconds for alarm
writeBuffer[2] = 0x51;   //minutes for alarm
writeBuffer[3] = 0x18;  //hours for alarm
writeBuffer[4] = 0x02; //day of week
writeBuffer[5] = 0x18; //date for alarm
writeBuffer[6] = 0x03;  //month for alarm
writeBuffer[7] = 0x19;  //year for alarm  

if(write(file, writeBuffer, 1)!=1){
perror("Failed to write in the buffer\n");
return 1;}
write(file,writeBuffer,8);
read(file,writeBuffer,8);
return 0;    
}

//Square wave generation
int Connection::SQWave(){
    writeBuffer[1] = {0x07};
    writeBuffer[1] = 0x11;    //writing the binary value 0b00010001 onto 0x07
    write(file,writeBuffer,8);
    read(file,writeBuffer,8);
    close(file);
    return 0;
}

//Function calling into main function
int main(){
Connection a;
a.Test();
a.Sensor();
a.Reset();
a.Display();
a.Write();
a.SQWave();
a.Alarm();
    
}